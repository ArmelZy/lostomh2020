/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mountainhack.losto.web.rest.vm;
